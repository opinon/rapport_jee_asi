---
author: ["Olivier Pinon", "Clément Gillet", "Loïc Ousset", "Julien Laurencin"]
date: "15 Mai 2019"
keywords: ["Java", "ASI", "JEE"]
title: Architecture des Systèmes d'Informations / JEE - CPE Lyon
titlepage: true
lang: fr-FR
bibliography: Architectures.bib
---

# Architecture SOA

SOA veut dire **Service Oriented Architecture**, ou **Architecture Orientée Services** en Français.

C'est un type d'architecture où chaque service est proposée aux composants de l'application par le biais d'un protocole de communication interne, le plus souvent à travers un réseau (on peut aussi faire passer les messages par les mécanismes divers d'`IPC` comme les `pipe`s). 

Un service est une unité indépendante qui possède ses propres fonctionnalités, et peut être accédé et modifié depuis un autre processus.

Un service a 4 propriétés principales [@soaopengroup], un service est donc : 

- Représente une logique métier avec une sortie spécifique
- C'est une boîte noire pour les consommateurs (on ne connaît pas les détails d'implémentation d'un service)
- Self-contained (son lancement ne dépend pas d'autres services)
- Cependant, il peut contenir d'autres services

Les services sont maintenus et deployés indépendamments les uns des autres, et sont distribués sur une unique machine physique, même si chaque service peut référencer d'autres services externes comme l'API `Twitter` par exemple.

# Architecture Microservices 

L'architecture Microservices est une variante moderne de l'architecture orientée service [@soawiki] spécialisée dans les systèmes distribués. La communication se fait uniquement par le réseau, et le protocole de communication est agnostique : on peut dialoguer par HTTP ou directement par TCP, UDP ... 

Cette architecture est devenue particulièrement populaire dans les années 2010 et l'introduction de la profession de `DevOps`[^devops]. Elle s'appuie sur des pratiques comme le déploiement continu - décentralisé, généralement - et autres principe agiles de développement. 

La plupart du temps, ces micro-services sont déployés dans des containers afin de permettre cette particularité de technologie complêtement agnostiques (puisque les services sont déployés sur différentes machines). Les services sont monitorés indépendamment, ce qui permet un contrôle plus poussé de chaque composant.

Un bon exemple d'application de l'architecture Microservices se trouve chez Netflix. Lien de l'article en bibliographie [@netflix].

[^devops]: Ensemble de pratiques logicielles intégrant le développement et le déploiement de services. 

# Architecture Monolithique

L'architecture Monolithique est une ancienne façon de développer les services Web, sans spécialisation particulière. Le principe de celle-ci consiste à regrouper sous forme de modules chaque service, et d'exposer toutes les routes sous un seul programme qui se chargera d'y répondre. 

Particulièrement utilisée auparavant, elle a la particularité de mettre en place de maninère générale un couplage relativement fort entre tous les composants, problème résolu par la mise en place des bons design patterns et du respect du principe d'encapsulation.

Le prototypage d'un petit projet est plus simple avec ce principe, puisque les classes sont toutes présentes au même endroit, et on peut ainsi faire un petit projet de test très simplement.

De même, le test coverage est plus simple à déterminer car il n'y a qu'un seul projet, et la mise en place de la CI[^ci] est facile et peu redondante, puisqu'il n'y a qu'un seul projet.

[^ci]: Intégration Continue, démarche de tests en continu au cours du développement

# Tableau récapitulatif

| -                            | SOA                      | MicroService             | Monolithique         |
| ---------------------------- | ------------------------ | ------------------------ | -------------------- |
| Modularité                   | Normale                  | Forte                    | Faible               |
| Scalabilité                  | Verticale uniquement     | Horizontale et verticale | Verticale uniquement |
| Monitoring                   | Par service              | Par service              | Unique               |
| Nombre de programmes         | Une pour chaque service  | Un pour chaque service   | Un seul              |
| Déploiement des services     | Indépendant              | Indépendant              | Unique               |
| Technologies utilisées       | Généralement unique      | Agnostique               | Unique               |
| Communication inter-services | IPC / Réseau             | Réseau                   | Interne              |
| Protocole inter-services     | Unique                   | Agnostique               | N/A                  |
| Couplage                     | Faible                   | Très faible              | Fort                 |
| Prototypage                  | Relativement long        | Relativement long        | Court                |
| Mise en place CI/CD          | Peu facile               | Facile                   | Très facile          |


# Bibliographie