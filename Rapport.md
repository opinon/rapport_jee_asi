---
author: "Olivier Pinon"
date: "15 Mai 2019"
keywords: ["Java", "ASI", "JEE"]
title: Architecture des Systèmes d'Informations / JEE - CPE Lyon
toc: true
toc-own-page: true
lang: fr-FR
---

# Atelier 1

## Pattern MVC : Model Vue Controler  

Le pattern MVC est une architecture logicielle qui :   

* Permet de séparer la couche métier des intéractions et de la vue 
* Respecte au mieux le principe de responsabilité unique
* Est reconnu comme étant viable et intuitif, appliqué depuis *très* longtemps

## Le Web Dynamique et Statique

Selon Wikipédia, `Une page web dynamique est une page web générée à la demande`.

Son contenu varie en fonctions d'informations diverses :  
 
* Heure 
* Nom de l'utilisateur
* Formulaires remplis par l'utilisateur
* etc...

Les données ne sont connues **qu'au moment de la consultation** de la page et donc la structure dela page peut varier.
A l'inverse, une page Web Statique ne varie *a priori* pas de structure ni de contenu entre deux consultations, c'est le browser qui vient ensuite faire varier le contenu grâce notamment à du JS et des requêtes AJAX.

* Le Web Dynamique est intéressant pour sa propriété de personnalisation du contenu à chaque utilisateur.
* Toutes les requêtes sont masquées à l'utilisateur puisqu'on ne sort qu'une seule donnée
* Intégrité maîtrisée des ressources que l'on donne à l'utilisateur (au prix d'une utilisation de ressources un peu plus intensive)
 
## Comment sont récupérés les fichiers par un web browser en web statique 

Typiquement, le dialogue entre le client et le serveur suit le schéma suivant : 

```{.svgbob width="80%"}

		  1.
 +-------------+  Requête HTTP GET ( URI )      +-------------+    2.
 | Web browser | ------------------------------>| HTTP Server |    Gérer la requête
 +-----------+-+                                +-------------+    (middleware, actions)
     o    ^  ^                                         |	   
    -+-   |  +-----------------------------------------+	   3. Construction de la response	
     |  --+	4. Retourne la response ( status, contenu )		(e.g. string, file, stream)
    / \						
    User

```

Le Web Server a pour but de délivrer des **réponses**. Ces dernières peuvent être :   

* Le contenu d'un fichier,
* Une string classique (e.g. pour transférer du JSON),

Selon l'objectif du Web Server, la réponse peut être construite en allant chercher le contenu d'un fichier sur le filesystem, en construisant une string JSON grâce à une requête, ou autre.  

Dans le cas du Web Statique, il s'agit de retourner un contenu qui sera tout le temps le même (vraisemblablement, un fichier HTML). L'identifiant de type de fichier - appelé **mime type**, est stocké dans le header de la requête HTTP.

Il sera alors de la responsabilité du Web browser et du JS présent sur la page de changer le contenu de la page en effectuant des requêtes AJAX.  

## Utiliser du Web Statique avec des services REST 

* "Moins" de données à transférer car seules les données utiles sont utilisées
* Principe de responsabilité unique mieux respecté : le back-end répond seulement aux routes, le front-end ne se charge que d'afficher les informations. Opposition au web dynamique, où le front-end ne fait que d'afficher les données issues du serveur, qui doit donc aussi gérer la vue. 
* Tests plus faciles du fait de la séparation des responsabilités.

## Comment fonctionne l'AJAX 

AJAX: Asynchronous Javascrtip And XML.

Il s'agit d'un ensemble de techniques utilisées uniquement côté front-end. Permet à un client web browser de récupérer des informations sur le web de manière asynchrone ("en tâche de fond", c'est à dire envoyer une requête et effectuer des opérations sans attendre directement la réponse).

On utilise l'objet JS `XMLHttpRequest` pour construire des requêtes AJAX en Javascript. On construit tout d'abord l'objet, puis on `open(method, uri)` la requête. On change ensuite la fonction callback `xhr.onreadystatechange`, afin de pouvoir effectuer des opérations avec le résultat de la requête. On envoie finalement la requête avec `send(body)`. Quand la requête est prête, la fonction onreadystatechange sera appellée et xhr.readyState sera égal à 4.

On trouve un exemple d'utilisation de l'API XMLHttpRequest dans le bout de code suivant : 

\newpage 

```javascript
// Crée l'objet XHR
var xhr = new XMLHttpRequest();
xhr.open('GET', 'mon_backend.jsp');

// Définit le callback
xhr.onreadystatechange = function() {
	if (xhr.readyState == 4) {
		console.log(xhr.status);
		console.log(xhr.responseText);
	}
} 

// Envoie la requête
xhr.send(null);
```

On peut aussi envoyer une requête AJAX avec la fonction `fetch(uri)`, ou en utilisant jQuery avec `$.ajax(params)`. On récupère alors une `Promise`, objet qui permet de chaîner des opérations une fois qu'une autre est terminée.

## JEE

### Qu'est ce que JEE et JSP   

JEE: Java Enterprise Edition.    
JSP: JavaServer Pages.  

JEE est une technologie Java adaptée pour écrire des web services.
On décrit le cycle de vie d'une application JEE dans le schéma ci-dessous :

```{.svgbob}

                   get / set    Bean    get / set
                  +----------> +----+ <----------+
                  |            |    |            |
+---+  appele   +---+          |    |          +---+ insert, update ...    ___
|  o| --------> |  o|          +----+          |  o| -------------------> (___)
|   |           |   |                          |   |                      (   ) MySQL, 
|   | <-------- |   | -----------------------> |   | <------------------- (   ) PgSQL,
+---'  redirect +---'    appelle méthodes      +---'       replies        (___) SQLite
 JSP            Servlet                         DAO                     Database

```

* **JSP** est un ficher de template, qui sert à gérer la vue. C'est le fichier directement affiché à l'utilisateur final.
* Un **Servlet** est la partie du controler qui sert à arranger les informations pour la vue, de manière plus générique à arranger le résultat de la réponse retournée à l'utilisateur.
* **Bean** est la partie du controler qui fait le lien avec la couche Model
* **DAO** est la couche métier, ce sont les classes de modèles qui font les interactions avec la base de données

### WebContainers JEE

Un WebContainer JEE est le composant d'un serveur web qui intéragit avec les Servlet. C'est le container qui est responsable de la mise en place du cycle de vie de l'application et des servlets. Par exemple, les WebContainer les plus connus sont les suivants :  

* Apache Tomcat
* GlassFish
* Jetty
* JBoss (maintenu par RedHat)

## EL et JSTL 

### EL: Expression Language  

EL donne accès à des fonctionnalités intéressantes permettant la communication entre la vue et la couche logique de l'application sous-jacente.

Par exemple, EL permet l'intégration de boucles et de **structures conditionnelles** (if) permettant d'aboutir à des fonctionnalités complètes comme l'i18n[^i18n]. Une documentation exhaustive d'EL est disponible sur [la documentation officielle](https://docs.oracle.com/javaee/6/tutorial/doc/bnahq.html).

### JSTL: JavaServer Pages Standard Tag Library  
 
JSTL est un ensemble de **tags** disponibles pour EL qui peuvent être classifiés comme suit : 

- Core: fonctionnalités principales
- SQL: Fonctionnalités de requêtage sur les bases de données à travers la vue. Visiblement, faut faire gaffe en l'utilisant.
- XML: interaction avec le DOM, structure du document HTML qu'on est en train de construire avec le JSP. Exemple: `forEach`, `if`, ...
- Fonctions: fonctions standards comme `length`, `replace`, `join`, ...

[^i18n]: Internationalisation, ensemble de techniques utilisées pour rendre le site ou l'application web utilisable dans toutes les régions. Pas seulement la traduction, mais aussi l'utilisation des références locales e.g. devise monétaire, système de mesure, culture de la région, ...

## JDBC: Java DataBase Connectivity 

JDBC est la suite de drivers qui permet d'entretenir une connexion avec un système de gestion de base de données. Cette API est majoritairement orientée vers les systèmes relationnels et est extensibles à tous les drivers qui fonctionnent sur la JVM[^jvm]. 

Statement et PrepareStatement sont des interfaces qui permettent d'envoyer des requêtes SQL ou PL/SQL sur la base de données à laquelle l'API JDBC est connectée. Comme en PHP, il est possible de **préparer** des requêtes, c'est à dire de mettre des paramètres dans la requête SQL qui seront évalués lors du lancement de celle-ci sur la base de données, dans le but de rendre l'appel réutilisable.

Différence notable : Statement est fait pour de one shot (un update sur la base, par exemple) alors que PreparedStatement est fait pour être réutilisé. 

[^jvm]: Java Virtual Machine, VM qui permet d'interpréter le bytecode Java (fichiers .class). Certains languages comme Scala et Kotlin utilisent cette VM comme cible de compilation pour bénéficier de l'aspect cross-platform offert par la JVM.

# Atelier 2 - SpringBoot

## Service Full REST

REST (REpresentational State Transfer) est une architecture logicielle imposant une suite de contraintes permettant d'établir une liaison entre ordinateurs, et donc d'effectuer des requêtes de manipulation de données. Pour utiliser ce type de service, on impose de passer par des requêtes HTTP. Les verbes HTTP utilisables sont :  

* GET 
* HEAD 
* POST
* PUT
* PATCH 
* DELETE 
* CONNECT 
* OPTIONS 
* TRACE  

Il y a 6 contraintes architecturales à la création d'un service RESTful : 

* Client-Serveur : les reponsabilités sont séparées 
* Sans état : pas de conservation d'état entre deux requêtes (mécanisme de session)
* Mise en cache : pour aller plus vite, un serveur ou un client peuvent mettre en cache la réponse afin d'éviter du travail inutile.
* En couches : un client ne peut pas définir s'il échange avec le serveur original ou un serveur intermédiaire ce qui permet le réplicat d'endpoints
* Avec code à la demande (facultatif) : les serveurs peuvent passer du code à éxécuter à leurs clients
* Interface uniforme pour standardiser le fonctionnement :
	* Identification des ressources de la requête par URI (e.g. l'ID d'un produit dans l'URL)
	* Manipulation des ressources par représentation (utilisation d'XML, JSON, ...)
	* Messages auto-descriptifs : chaque message peut être compris de lui-même, par exemple en utilisant les *mime type* pour décrire le document qu'on retourne
	* Hypermédia comme moteur d'état de l'application : après avoir découvert l'URL du service - sa **page d'accueil** - un client doit pouvoir utiliser les hyperliens fournit dans les documents pour naviguer à l'intérieur (ce qu'on appelle **HATEOAS**) 

## Gestionnaire de dépendances 

### Trivia 

Un gestionnaire de dépendances est un *outil tiers* qui permet de gérer les librairies externes sur lesquelles une application dépend. Il permet d'**inclure**, **modifier** la version ou **supprimer** une dépendance sur toute librairie disponible, et facilite ainsi l'utilisation de code tiers.

Maven n'est évidemment pas le seul gestionnaire de dépendances, on trouve par exemple :  
 
- Gradle (Java) 
- npm (JavaScript)
- Composer (PHP)
- Cargo (Rust)

### Fonctionnement de Maven 

A chaque lancement, Maven lit la liste de dépendances du projet et tente de les résoudre, i.e. définir quelle version de la librairie est nécéssaire pour satisfaire toutes les librairies qui en dépendent.   

Maven tente suite de télécharger tout ce qu'il ne possède pas dans son cache local.  

On décompose l'utilisation de Maven avec ces commandes : 
* compile : Compile les sources du projet
* test : Lance les tests unitaires du projet (utile dans une démarche de CI[^CI] notamment
* install : Installe le package dans le repository local pour l'utiliser comme dépendance dans d'autre projets. 
* deploy : Déploie le paquet sur le remote repository pour être utilisable par d'autres dévelopeurs. 

[^CI]: Continuous Integration, ou Intégration Continue en Français, est une pratique de développement qui permet de lancer les tests ou le contrôle de qualité, suivi parfois du déploiement automatique de la solution. 

## Enterprise Java Application 

Une application qui utilise la plateforme JEE est une Enterprise Java Application (**EJA**).

On peut par exemple :   

* Créer un WebService (comme vu en TP), 
* Créer des pages web dynamiques avec JSP, 
* Créer des services complets avec JavaBean,

**EJB** est l'interface entre l'EJA et le serveur Java EE. **JMS** pour Java Message Service est un middleware[^middle] permettant d'envoyer un message entre deux (ou plus) clients.

[^middle]: Un programme qui permet de donner accès à des services de niveau supérieur à l'OS. C'est une sorte de "glue logicielle"

## Spring & SpringBoot

### Trivia 

Spring est un Framework pour la plateforme Java EE, orienté pour la création de services utilisant une base de données, résolument orienté sur l'architure REST.

SpringBoot est un outil tiers permettant de créer et lancer des projets créés avec Spring.

*NB: la question du TP demande les points communs et différences entre JEE et SpringBoot, ce qui ne fait pas de sens puisque SpringBoot est un outil. Je donne donc la différence entre Spring et JEE* 

La différence entre Spring et JEE réside dans le fait que Spring est un Framework pour la plateforme JEE, qui quant à elle représente le côté "logiciel" serveur.

### Créer un Web Service REST avec SpringBoot 

Pour créer un WebService avec Spring, on va se servir des annotations. On crée une classe taguée Controller, avec l'annotation `@RestController`.

Cette classe controller définit les routes disponibles pour l'API avec l'annotation `@RequestMapping` et il est possible d'assigner une valeur dans l'URL avec l'annotation `@PathVariable`

```java 
@RestController
public class UserController {
	// [Contenu ...]
	
	@RequestMapping(method=RequestMethod.POST,value="/cardsUsers")
	public void addCard(@RequestBody Users user, @RequestBody Card card) {
		// appel au Service
	}

} 
```

On crée ensuite une classse service qu'on va taguer avec l'annonation `@Service`. Ce service contient les appels aux Repository. 

```java 
@Service
public class UserService {
	// [Contenu ...]
}
``` 

On vient ensuite lier un Service avec un controller en utilisant l'annotation `@Autowired`

```java 
public class UserController {
	@Autowired
	public UserService userService;
} 
```

On crée finalement un Repository, la classe qui est chargée de créer le lien entre la couche métier et la base de données

```java
public class UserRepository extends CrudRepository<User, Integer> {
	// [Contenu ...]
}
```

Et pareil que précédemment, on fait le lien entre le repository et le service en utilisant le tag @Autowired dans le Service. 

```java
public class UserService { 
	@Autowired
	public UserRepository userRepo;
}
```

Au final, on peut faire le schéma suivant pour l'architecture de ce WebService: 

```{.svgbob} 
													     ___ 
+--+                    +--+             +--+           (___)
| o| -----------------> | o| ----------> | o| --------> (   )
|  |                    |  |             |  |           (   )
+--'                    +--'             +--'           (___)

Controller              Service          Repository      BDD
Matching de route       Couche métier    Couche ORM 
```

### Convention over configuration

Il s'agit d'un paradigme de design qui vise à réduire le nombre de décisions qu'un développeur utilisant le Framework a à faire, sans sacrifier la fléxibilité de celui-ci. 
Cela veut dire qu'un développeur devra spécifier uniquement les aspects non-conventionnels du programme qu'il développe. Par exemple, une convention est de nommer les tables en BDD et les classes de la même manière (classe `Sales` pour la table `sales`). C'est seulement si la table dérive de cette convention, par exemple parce qu'on appelle notre classe `ProductSales` que le développeur devra écrire du code pour changer cet aspect dans l'application.  

### Que se passe-t-il quand on lance SpringApplication.run(App.class, args) ?

En utilisant cette fonction:   

- Spring démarre un Contexte d'application,
- L'utilise afin d'executer la mécanique d'**autodiscovery**. Celle-ci scan les classes présentes dans l'application et crée ainsi les controllers, services et relations avec la base de données spécifiées par le développeur. 
- Toutes les configurations par défaut de spring sont appliquées, 
- Enfin, un container de servlet est déployé pour l'application, rendant le service actif

## Annotations Java

En Java, une annotation définit toute partie de la syntaxe qui démarre pas un `@`. Les annotations servent à donner des informations au compilateur, et c'est en général ce qu'on utilise pour faire fonctionner les frameworks en leur passant des métadonnées sur les champs.

Par exemple, dans Spring on utilise les annotations :  

* `@Entity`
* `@Id`
* `@ManyToMany`

## Servlets 

Un Servlet est un composant Java qui étend les capacités d'un serveur en répondant à des requêtes HTTP que son container lui envoie. C'est l'équivalent d'une technologie de Web Dynamique comme par exemple `PHP` et `ASP.net`.

## Web Container 

Un Web Container ou Servlet container est un composant de serveur Web qui intéragit notamment avec les servlets définit précédemment. Le container Web est responsable de : 

- Gérer le cycle de vie de ses Servlets, 
- Mapper les URL aux servlets pour leur envoyer les requêtes HTTP concernées,
- Vérifier les droits d'accès à une URL  

De manière générale, un container fait partie de l'architecture de Java EE et spécifie l'environnement pour ses composants, notamment : 

- La sécurité,
- L'aspect concurrentiels (*threads*),
- La gestion du cycle de vie, 
- Les principes transactionnels,
- Le déploiement 

Les servlets ne sont pas les seuls composants stockés dans un web container, on trouve aussi les JavaServer Pages (JSP) ou tout autre fichier possédant du code executable côté serveur.

Dans la liste des Web Containers on trouve notamment : 

- Apache Tomcat
- Apache Geronimo 
- GlassFish
- Jetty

## DAO 

DAO veut dire Data Access Object. C'est une abstraction qui permet de découpler la source d'un objet de son utilisation. Le DAO agit comme une interface avec la base de données, sur laquelle on va en général définir `save` pour enregistrer l'objet en base, et `read` pour le récupérer depuis celle-ci. Il est possible de le transformer avec un Mapper en objet qui possède les propriétés dont on a besoin dans la logique métier. Par exemple, dans le cadre du TP vu en cours, nous avons besoin de sauvegarder des cartes dans la base de données et de les faire combattre. On peut ainsi découpler cette partie de l'application de la façon suivante : 

```{.svgbob}

+--------------------+  |  Librairie commune  |         Service de création des cartes    
|					 |  |                     | 
| Service combat 	 |	|				      | +--------+
| (carte 1, carte 2) |--+--------+    		  | | Mapper |						  ___
|					 |	|	  	 |			  | +--------+						 (___)
+--------------------+	|	  +-----------+	  |     | |	     +---------+		 (   )
						|	  |	Card	  |<--+-----+ +----->| CardDAO | <-----> (   ) BDD
+--------------------+	|	  +-----------+	  |  fn map_to	 +---------+		 (___)
|					 |  |        |    	      | 							
| Service affichage	 |--+--------+    	      | 								 
| (carte)			 |  |                     | 
|					 |  |                     | 
+--------------------+  |                     | 

```

Card est alors disposée dans une librairie commune, et aucun des services n'a besoin de connaître les détails d'implémentation qui lit une Card à la base de données. 

Un `Singleton` est un *design pattern* qui permet de garantir un accès global et unique à une instance d'une Classe. C'est notamment utilise dans le cas de l'accès à une base de données, on peut utiliser `Database.getInstance()` afin de récupérer un objet Database, qu'on utilise par la suite pour envoyer des requêtes. 

Les `@Entity` de SpringBoot créent automatique les DAO en se basant sur la définition d'un objet, en mappant chaque champ de l'objet à une colonne en base de données.  
Ce n'est bien sûr pas spécifique à SpringBoot. Le travail réalisé ici est celui d'un outil qu'on appelle la couche `ORM` (Object-Relational Mapping). Il en existe beaucoup d'autres, par exemple : 

- en `PHP` on trouve `Doctrine`, 
- en C# on trouve `Entity Framework`,
- en Rust on trouve `Diesel`  

Lors de l'utilisation d'un service, nous avons crée toutes les instances qui étaient reliées à la classe par l'annotation `@Autowired` (donc une, à savoir le Repository). Selon la requête, un service peut ne créer aucune instance de DAO s'il n'y a pas besoin de se connecter à la base de données.

## CRUD Repository 

CRUD signifie `Create Read Update Delete`. C'est en général une implémentation des 4 fonctions qui permettent d'intéragir avec la base de données. Le CRUD Repository de Spring est une généralisation du concept de CRUD à toutes les classes, ce qui permet d'obtenir ces méthodes pour toute classe sérialisable.    

D'une manière générale, Spring et ses méthodes `findByProperty` permettent de la même sorte d'éviter la duplication de code. Elles réalisent simplement l'équivalent de la requête SQL suivante : 

```sql 
SELECT champs 
FROM ma_table
WHERE property = 'argument de findByProperty';
```